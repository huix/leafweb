class Config(object):
    SITE_NAME='Leaf Diagnosis Demo'
    SITE_SLUG_NAME='leaf-diagnosis'
    SITE_LOCATION='Outer space'
    TAGLINE='Diagnosis plant with AI'
    TAGLINES=['Leaf diagnosis using Deep Learning']
    SITE_DESCRIPTION='Leaf diagnosis'
    SITE_KEYWORDS='leaf diagnosis, deep learning'
    GOOGLE_SITE_VERIFICATION=' X'
    FACEBOOK_PAGE_ID=''
    TWITTER_ID=''
    #  google_plus_id='X'
    GOOGLE_ANALYTICS=' UA-x'
    ADMINS=['xh@disroot.org']

class DevelopmentConfig(Config):
    DOMAIN= 'localhost=5000'
    ASSET_DOMAIN= 'localhost=5000'

class ProductionConfig(Config):
    DOMAIN= 'www.leaf-diagnosis.com'
    ASSET_DOMAIN= 'www.leaf-diagnosis.com'
