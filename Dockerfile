FROM python:3.5-slim
RUN pip3 --no-cache-dir install --upgrade pip setuptools && pip install numpy pillow && rm -rf /root/.cache && rm -rf /var/cache/apk/* && rm -rf /root/.[acpw]*
 
# Pytorch CPU
RUN pip --no-cache-dir install --upgrade http://download.pytorch.org/whl/cpu/torch-0.3.1-cp35-cp35m-linux_x86_64.whl && rm -rf /root/.cache && rm -rf /var/cache/apk/* && rm -rf /root/.[acpw]*
RUN pip --no-cache-dir install --upgrade torchvision && rm -rf /root/.cache && rm -rf /var/cache/apk/* && rm -rf /root/.[acpw]*

# Necessary packages
ADD ./requirements.txt  /temp/requirements.txt
RUN pip --no-cache-dir install --upgrade -qr /temp/requirements.txt && rm -rf /root/.cache && rm /temp/requirements.txt && rm -rf /var/cache/apk/* && rm -rf /root/.[acpw]*

# Add our code
ADD ./webapp /opt/webapp/
WORKDIR /opt/webapp

# Run app.py when the container launches
#CMD ["python", "/opt/webapp/app.py"]
CMD gunicorn --bind 0.0.0.0:$PORT --timeout 120 wsgi
