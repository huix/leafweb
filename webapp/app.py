from __future__ import print_function, division
import torch
from torch.nn import functional as F
from torchvision import models
import io
from numpy import array, empty, vstack
#import cv2
from get_result_file_paths import get_result_file_paths
from ResNet18Model import ResNet18FC
#from resnet18_run import share_parser
from visual_tools import guided_gradcam, make_grid
from flask import Flask
from flask import request
from flask import jsonify
from PIL import Image, ImageDraw, ImageFont
from flask import send_file
from flask_mako import MakoTemplates, render_template
from plim import preprocessor
from collections import OrderedDict

app = Flask(__name__, instance_relative_config=True)
# For Plim templates
mako = MakoTemplates(app)
app.config['MAKO_PREPROCESSOR'] = preprocessor
app.config.from_object('config.ProductionConfig')


usecuda = False
if usecuda:
    torch.backends.cudnn.benchmark = True

data_separate = '50-50'

model_dir='model_example'
logits_dir=''
archname='resnet18'
has_weights='False'
is_soft_target='False'
is_distilled='MSETrue'
data_type='test'
epoch = 29
wd = 0.0005
datasetname = 'leaf'
lr = 0.0006

model = models.__dict__[archname]() #pretrained=True
model.eval()
distilled_model = ResNet18FC(model)
distilled_model.eval()
if usecuda:
    distilled_model = torch.nn.DataParallel(distilled_model).cuda()

del model
model_path, preds_labels_file = get_result_file_paths(epoch, lr, wd, datasetname, data_separate, archname, data_type, has_weights, logits_dir=logits_dir, model_dir=model_dir, is_soft_target=is_soft_target, is_distill=is_distilled)
"""
print('loading %s ' % model_path)
state_dict = torch.load(model_path)['state_dict']
print('load %s ' % model_path)

state_dict_rename = OrderedDict()
for k, v in state_dict.items():
    name = k[7:] # remove `module.`
    state_dict_rename[name] = v.cpu()
"""
state_dict_rename = torch.load(model_path)
distilled_model.load_state_dict(state_dict_rename)
#distilled_model.load_state_dict(state_dict)
distilled_model.eval()

fo = open('labels.txt', 'r')
classes = []
for l in fo:
    classes.append(l.split('\n')[0])
fo.close()
classes_to_ind_dict = {}
for i, cla in enumerate(classes):
    classes_to_ind_dict[cla] = i


def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

@app.route('/predict', methods=['POST'])
def predict():
    # draw the system preview resnet18 visualization
    
    if archname == 'resnet18':
        target_layer = 'layer4/1/relu'
    elif archname == 'resnet101':
        target_layer = 'layer4/2/relu'
    result_imgs = []
    predict_list = []
    target_list = []
    prob_list = []

    #selected_path = request.get('path')
    selected_path = request.files['file']

    img_with_gradcamplus_heatmap, original_image, grayscale_cam_gb_plus_img, predic_ind, target_class, model_out, _=guided_gradcam(distilled_model,target_layer, img_dataset_loader=None, batch_ind=None, img_ind=None, base_model=None, draw_feature_num=0, draw_acc_pred=True, data_sep=data_separate, given_img_path=selected_path, classes_to_ind_dict=classes_to_ind_dict, usecuda=usecuda, has_target_labels=False)
    #grayscale_cam_gb_img = grayscale_cam_gb_img.unsqueeze(dim=0)
    grayscale_cam_gb_plus_img = grayscale_cam_gb_plus_img.unsqueeze(dim=0)
    #grayscale_cam_gb_img = torch.cat((grayscale_cam_gb_img, grayscale_cam_gb_img, grayscale_cam_gb_img), 0)
    grayscale_cam_gb_plus_img = torch.cat((grayscale_cam_gb_plus_img, grayscale_cam_gb_plus_img, grayscale_cam_gb_plus_img), 0)
    result_imgs.extend([original_image, img_with_gradcamplus_heatmap, grayscale_cam_gb_plus_img])
    #result_imgs.extend([original_image, img_with_gradcam_heatmap, grayscale_cam_gb_img, img_with_gradcamplus_heatmap, grayscale_cam_gb_plus_img])
    predict_list.append(predic_ind)
    if target_class is not None:
        target_list.append(target_class)
    else:
        target_list = None

    pred_prob = F.softmax(model_out.cpu(), dim=1).data.max()
    pred_prob = float("{0:.5f}".format(pred_prob))
    if target_class is not None:
        result_txt = 'predict: '+classes[predic_ind]+' / label: '+classes[target_class]+'; probability: '+str(pred_prob)
        #return jsonify({'prediction': classes[predic_ind], 'label': classes[target_class], 'probability': pred_prob})
    else:
        result_txt = 'predict: '+classes[predic_ind]+'; probability: '+str(pred_prob)
        #return jsonify({'prediction': classes[predic_ind], 'probability': pred_prob})
    prob_list.append(pred_prob)

    out = make_grid(result_imgs, nrow=3, pad_value=0)
    out = out.numpy().transpose(1,2,0)
    text_img_np = empty((20, out.shape[1], out.shape[2]),dtype=out.dtype)
    text_img_np.fill(255)
    text_img = Image.fromarray(text_img_np)
    draw = ImageDraw.Draw(text_img)
    font = ImageFont.truetype("DejaVuSans.ttf", 18)
    draw.text((0, 0),result_txt,(0,0,0), font=font)
    text_img_np = array(text_img)
    #print(text_img_np.shape)
    out = vstack((text_img_np, out))
    #out = cv2.cvtColor(out,cv2.COLOR_BGR2RGB)
    outimg = Image.fromarray(out)
    byte_io = io.BytesIO()
    outimg.save(byte_io, 'PNG')
    byte_io.seek(0)
    return send_file(byte_io, mimetype='image/png')
    #return jsonify({'prediction': classes[predic_ind], 'probability': pred_prob})
    
@app.route('/')
def homepage():
    return render_template('index.html.slim', name='mako')


if __name__=='__main__':
    app.run(host='0.0.0.0', port=80)
